from django.apps import AppConfig


# config de seguimiento para el admin
class SeguimientoConfig(AppConfig):

    name = 'Consultas'
    verbose_name = 'Gestión de Consultas'
# este es el final de la linea
