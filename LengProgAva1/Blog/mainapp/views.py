from django.shortcuts import render
from .models import Habilidades

# Create your views here.
def index(request):

    personas=Habilidades.objects.all()
    return render(request, 'mainapp/index.html', {
        'title': 'Home',
        'pagina':'Bienvenidos',
        'personas':personas
    })
